//
//  ArticleViewModel.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/14/20.
//

import Foundation
import SwiftyJSON
import Alamofire

struct ArticleViewModel {
    
    static let shared = ArticleViewModel()
    
    
    // MARK: - func fetch data
    func fetchDataTest(fetchPage: Int? = 1,completion: @escaping ([ArticleModel],[Int])->()) {
        Service.share.fetchDataTest(fetchpage: fetchPage!){ (article) in
            var articleModels = [ArticleModel]()
            for item in article.data{
//                print("12345",item)
                articleModels.append(ArticleModel(article: item))
            }
//            articleModels = article.compactMap(ArticleModel.init)
//            print(articleModels[0].)
//            print(article.totalPage, article.page)
            completion(articleModels, [article.totalPage!,article.page!])
            
        }
    }
    
    // MARK: - func fetch data
    func fetchData(completion: @escaping ([ArticleModel])->()) {
        Service.share.fetchData { (article) in
            var articleModels = [ArticleModel]()
            articleModels = article.compactMap(ArticleModel.init)
//            print()
            completion(articleModels)
            
        }
    }
    // MARK: - func delete data
    func deleteData(id:String, completion: @escaping (Message)->()){
        Service.share.deleteData(id: id, completion: { message in
            completion(message)

        })
    }
    // MARK: - func add new data
    func addNewData(upload:Upload, completion: @escaping (Message)->()){
        Service.share.addNewData(upload: upload, completion:{ message in
            completion(message)
//            print("hello 123456")
        })
    }
    // MARK: - func upload image to api
    func uploadImageToAPI(image: UIImage, completion:  @escaping (ImageUpload)->()){
        Service.share.uploadImageToAPI(image: image, completion: {message in
           completion(message)
        })
    }
    // MARK: - func update data
    func updateData(id: String, upload:Upload, completion: @escaping(Message)->()){
        Service.share.updateData(id: id, upload: upload) { message in
            completion(message)
        }
    }
}
