//
//  ArticleTableViewCell.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/14/20.
//

import UIKit
import Kingfisher

class ArticleTableViewCell: UITableViewCell {
    @IBOutlet weak var cat: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    // MARK: - func bind data
    func getData(articleModel:ArticleModel) {
        caption.text = articleModel.title
        dateLabel.text = articleModel.createdAt
        let url = articleModel.image ?? "default"
        let imgPost = url.contains(".png")||url.contains(".jpg")||url.contains("jpeg")
        if imgPost == true{
            let url = URL(string: articleModel.image!)
            postImage?.kf.setImage(with: url, placeholder: UIImage(named: "bear"),options: nil,progressBlock: nil,completionHandler: nil)
            
        }else{
            postImage.image = UIImage(named: "default")
        }
        // MARK: - array random image
        let imagesArray = [ "pan","panpan","bear","ice1","ice2",
                            "ice3","ice4","pan1","pan2","pan3","pan4","pan5",
                            "ton1","ton2","ton3","ton4","ton5",
        ]
        let imgURL = articleModel.author?.imageProfile ?? "pan"
//        print(imgURL)
        let img = imgURL.contains(".png")||imgURL.contains(".jpg")||imgURL.contains("jpeg")
        if img == true{
            let urlProfile = URL(string: (articleModel.author?.imageProfile!)!)
            profile.kf.setImage(with: urlProfile,placeholder: UIImage(named: "panpan"),options: nil,progressBlock: nil,completionHandler: nil)
        }else{
            profile.image = UIImage(named: imagesArray.randomElement()!)
        }
        
        //                profile.image = UIImage(named: "pan")
        
        category.text = "Category"
        // MARK: - array random name
        let names = ["Arthur", "Ford", "Zaphod", "Marvin", "Trillian","Kevin","Jane","Wathana","Pual","Natalia","Liza","Lily","Vanilla"]
        name.text = articleModel.author?.name ?? names.randomElement()
        cat.text = articleModel.category?.name ?? "None"
        
    }
}

