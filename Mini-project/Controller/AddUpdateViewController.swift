//
//  AddUpdateViewController.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/15/20.
//

import UIKit
import Kingfisher

class AddUpdateViewController: UIViewController, SendData,UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    var imagePicker:UIImagePickerController?
    var isImageUpdated:Bool = false
    var postImage:UIImage?
    var des: String = ""
    var tit: String = ""
    var d: String = ""
    var id: String = ""
    var nameUser: String = ""
    var imagePost: String = ""
    var proImg: String = ""
    var action: String = ""
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var desAdd: UITextView!
    @IBOutlet weak var titleAdd: UITextView!
    @IBOutlet weak var imgUpload: UIImageView!
    
    
    var articleModels:[ArticleModel]?
    var articleViewModel:ArticleViewModel?
    
    override func viewWillAppear(_ animated: Bool) {
        titleLabel.text = "tit".localizedString()
        descriptionLabel.text = "des".localizedString()
        desAdd.text = des
        titleAdd.text = tit
        let imgPost = imagePost.contains(".png")||imagePost.contains(".jpg")||imagePost.contains("jpeg")
        if imgPost == true{
            let url = URL(string: imagePost)
            imgUpload?.kf.setImage(with: url, placeholder: UIImage(named: "bear"),options: nil,progressBlock: nil,completionHandler: nil)
            
        }else{
            imgUpload.image = UIImage(named: "default")
        }
        if action != "update"{
            navigationItem.title = "Add Article".localizedString()
            addBtn.setTitle("Save".localizedString(), for: .normal)
        }else{
            navigationItem.title = "Edit Article".localizedString()
            addBtn.setTitle("Update".localizedString(), for: .normal)
            
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgUpload.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
        
        let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped))
        imgUpload.addGestureRecognizer(gestureRecognizer1)
        //        Service.share.updateData(id: "5fd9d2a506a732af92fd63c7", upload: Upload(title: "hi", description: "h", published: true, image: "")) { (message) in
        //            print(message)
        //        }
        
        
    }
    // MARK: - obj-c func tap image
    @objc func imageViewTapped(sender: UITapGestureRecognizer) {
        if let imageView = sender.view as? UIImageView {
            
            imageView.backgroundColor = .white
            self.imagePicker = UIImagePickerController()
            self.imagePicker?.allowsEditing = false
            self.imagePicker?.sourceType = .photoLibrary
            self.imagePicker?.delegate = self
            
            self.present(self.imagePicker!,animated: true, completion: nil)
        }
    }
    // MARK: - func pick image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.imgUpload.image = image
            self.postImage = image
            isImageUpdated = true
        }
        self.imagePicker?.dismiss(animated: true, completion: nil)
    }
    // MARK: - func upload data
    func UploadData(url:String) {
        ArticleViewModel.shared.addNewData(upload: Upload(title: self.titleAdd.text, description: self.desAdd.text, published: true, image: url), completion: { message in
            //            print("added")
            let alert = UIAlertController(title: nil, message: message.message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "done".localizedString(), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        })
        
    }
    // MARK: - func update data
    func updatedata(url:String){
        ArticleViewModel.shared.updateData(id: id, upload: Upload(title: self.titleAdd.text, description: self.desAdd.text, published: true, image: url )) { message in
            let alert = UIAlertController(title: nil, message: message.message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "done".localizedString(), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    // MARK: - func did send frm delegate
    func didSend(id: String, title: String, description: String, img: String, date: String, name: String, profile: String, action: String) {
        self.id = id
        self.tit = title
        self.des = description
        self.d = date
        self.nameUser = name
        self.imagePost = img
        self.proImg = profile
        self.action = action
    }
    
    // MARK: - func add
    @IBAction func addClick(_ sender: Any) {
        // MARK: - check condition
        
        if isImageUpdated == true{
            if action != "update"{
                ArticleViewModel.shared.uploadImageToAPI(image: postImage!, completion: {message in
                    self.UploadData(url: message.url)
                })
            }else{
                ArticleViewModel.shared.uploadImageToAPI(image: postImage!, completion: {message in
                    self.updatedata(url: message.url)
                })
                
            }
            
        }else{
            if action != "update"{
                ArticleViewModel.shared.uploadImageToAPI(image: postImage!, completion: {message in
                    self.UploadData(url: "string img")
                })
            }else{
                
                self.updatedata(url: self.imagePost)
                
            }
            
        }
        
        
        //        let alert = UIAlertController(title: "", message: "Are you sure to add this data?", preferredStyle: .alert)
        //        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { _ in
        //
        //            ArticleViewModel.shared.addNewData(upload: Upload(title: self.titleAdd.text, description: self.desAdd.text, published: true, image: ""), completion: { (message) in
        //                print("===========add done=========")
        //
        //                let addAert = UIAlertController(title: nil , message: message.message, preferredStyle: .alert)
        //                addAert.addAction(UIAlertAction(title: "done", style: .default, handler: nil))
        //                self.present(addAert, animated: true, completion: nil)
        //            })
        //        }))
        //        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        //        self.present(alert, animated: true, completion: nil)
        //    }
        
    }
    
}
