//
//  DetailViewController.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/15/20.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class DetailViewController: UIViewController,SendData {
    
    @IBOutlet weak var textDispaly: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    // MARK: - declare variable
    var des: String = ""
    var tit: String = ""
    var d: String = ""
    var id: String = ""
    var nameUser: String = ""
    var imagePost: String = ""
    var proImg: String = ""
    
    var delegate: SendData?
    var articleModels:[ArticleModel]?
    var articleViewModel:ArticleViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        textDispaly.text = des
        titleText.text = tit
        dateLabel.text = d
        name.text = nameUser
        let imgPost = imagePost.contains(".png")||imagePost.contains(".jpg")||imagePost.contains("jpeg")
        if imgPost == true{
            let url = URL(string: imagePost)
            img?.kf.setImage(with: url, placeholder: UIImage(named: "bear"),options: nil,progressBlock: nil,completionHandler: nil)
            
        }else{
            img.image = UIImage(named: "default")
        }
        let imgProfile = proImg.contains(".png")||proImg.contains(".jpg")||proImg.contains("jpeg")
        if imgPost == true{
            let url = URL(string: proImg)
            profile?.kf.setImage(with: url, placeholder: UIImage(named: "bear"),options: nil,progressBlock: nil,completionHandler: nil)
            
        }else{
            profile.image = UIImage(named: "default")
        }
        
        
    }
    // MARK: - func did send
    func didSend(id:String,title: String,description:String,img:String,date:String,name:String,profile:String, action: String){
        self.id = id
        //        print(id)
        self.tit = title
        self.des = description
        self.d = date
        self.nameUser = name
        self.imagePost = img
        self.proImg = profile
    }
    
    // MARK: - func edit update
    @IBAction func editClick(_ sender: Any) {
        let editView = self.storyboard?.instantiateViewController(identifier: "AddUpdateViewController") as! AddUpdateViewController
        self.delegate = editView
        self.delegate!.didSend(id: id, title: tit, description: des, img: imagePost, date: d, name: nameUser, profile: proImg, action: "update")
        self.navigationController?.pushViewController(editView, animated: true)
    }
    // MARK: - func delete 
    @IBAction func deleteClick(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Are you sure to delete this data?".localizedString(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes".localizedString(), style: .destructive, handler: { _ in
            ArticleViewModel.shared.deleteData(id: self.id, completion:{ message in
                //                print("====Data has been delete from api====")
                
                let delete = UIAlertController(title: nil, message: message.message, preferredStyle: .alert)
                delete.addAction(UIAlertAction(title: "done".localizedString(), style: .default, handler: {_ in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                self.present(delete, animated: true, completion: nil)
            })
            
        }))
        alert.addAction(UIAlertAction(title: "No".localizedString(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
}
