//
//  NotificationViewController.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/18/20.
//

import UIKit

class NotificationViewController: UIViewController {

    @IBOutlet weak var notification: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = "Notification".localizedString()
        notification.text = "Notification".localizedString()
    }
   
}
