//
//  ProfileViewController.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/18/20.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var choosetheme: UILabel!
    @IBOutlet weak var changeLang: UILabel!
    @IBOutlet weak var totalPost: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    func setLang(lang: String){
        AppService.shared.choose(language: lang)
        navigationItem.title = "Profile".localizedString()
        changeLang.text = "language".localizedString()
        totalPost.text = "totalPost".localizedString()
        nameLabel.text = "name".localizedString()
        logout.setTitle("logout".localizedString(), for: .normal)
        choosetheme.text = "chooseTheme".localizedString()
            }
    
    
    @IBAction func switchLang(_ sender: UISwitch) {
        if sender.isOn == false{
            self.setLang(lang: language.khmer.rawValue)
        }else{
            self.setLang(lang: language.english.rawValue)
        }
    }
}
extension String {
    func localizedString()->String {
        let path = Bundle.main.path(forResource: AppService.shared.language, ofType: "lproj")!
        let bundle = Bundle(path: path)!
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: self, comment: self)
    }
}
