//
//  ViewController.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/13/20.
//

import UIKit

// MARK: - protocol send data
protocol SendData {
    func didSend(id: String,title: String,description: String,img:String,date: String,name:String,profile:String, action: String)
}
class ViewController: UIViewController {
    
    @IBOutlet weak var articleTableView: UITableView!
    
    var articleModels:[ArticleModel]?
    var articleViewModel:ArticleViewModel?
    var delegate: SendData?
    var arrPage = [Int]()
    var fetchingPage: Int = 1
    
    var fetchMore: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        articleTableView.tableFooterView = UIView(frame: .zero)
        registerCell()
//        getData()
        
        
        articleTableView.refreshControl = UIRefreshControl()
        articleTableView.refreshControl?.addTarget(self, action: #selector(didPullRefresh), for: .valueChanged)

        fetchTest()
    }
    func fetchTest(fetchingPage: Int? = 1) {
        ArticleViewModel.shared.fetchDataTest(fetchPage: fetchingPage,completion: {res, getPage  in
//            print(res)
            if self.fetchMore == true {
                self.articleModels?.append(contentsOf: res)
            } else{
                self.articleModels = res
            }
            
            self.arrPage = getPage
            self.articleTableView.reloadData()
            
        })
    }
    @objc func didPullRefresh() {
//        getData()
        fetchTest()
        DispatchQueue.main.async {
            self.articleTableView.refreshControl?.endRefreshing()
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = "Popular Articles".localizedString()
//        getData()
    }
    
    // MARK: - func fetch data
    func getData(){
        self.articleViewModel = ArticleViewModel()
        self.articleViewModel?.fetchData(completion: { (articleModels) in
            //            print(articleModels[0].createdAt)
            self.articleModels = articleModels
            DispatchQueue.main.async {
                self.articleTableView.reloadData()
                
            }
        })
    }
    //    func deleteData(id: String){
    //        print("workkkk")
    //        ArticleViewModel.shared.deleteData(id: id, completion:{ message in
    //            let alert = UIAlertController(title: "", message: "Are you sure to delete this data?", preferredStyle: .alert)
    //            alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { _ in
    //                let delete = UIAlertController(title: nil, message: message.message, preferredStyle: .alert)
    //                delete.addAction(UIAlertAction(title: "done", style: .default, handler: nil))
    //                self.present(delete, animated: true, completion: nil)
    //            }))
    //            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
    //            self.present(alert, animated: true, completion: nil)
    //
    //            print(message.message)
    //        })
    //
    //    }
    
    // MARK: - func registerCell
    func registerCell(){
        articleTableView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "articleCell")
    }
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        if segue.identifier == "showDetail" {
    //            let dest = segue.destination as! DetailViewController
    //            dest.articleModels = sender as? [ArticleModel]
    //        }
    //
    //    }
    
}
// MARK: - extension
extension ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let articleModels = self.articleModels{
            return articleModels.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = articleModels!.count - 1
        if lastItem == indexPath.row {
            self.fetchMore = true
            if arrPage[1] > arrPage[0] {
                return
            }else{
                self.fetchingPage = fetchingPage + 1
                fetchTest(fetchingPage: fetchingPage)
            }
            
        }
    }
    // MARK: - func cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "articleCell", for: indexPath) as! ArticleTableViewCell
        let articleModels = self.articleModels![indexPath.row]
        cell.getData(articleModel: articleModels)
        return cell
    }
    // MARK: - func didSelectRowAt
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let secondView = storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
        self.delegate = secondView
        self.delegate?.didSend(id: self.articleModels![indexPath.row].id!
                               ,  title: self.articleModels![indexPath.row].title!
                               , description: self.articleModels![indexPath.row]
                                .description!, img: self.articleModels![indexPath.row]
                                    .image ?? "default", date: self.articleModels![indexPath.row]
                                        .createdAt!, name: (self.articleModels![indexPath.row].author?.name) ?? "Jane",
                               profile: self.articleModels![indexPath.row]
                                .author?.imageProfile ?? "default", action: "save");        self.navigationController?.pushViewController(secondView, animated: true)
        //        let articleModels = self.articleModels![indexPath.row]
        //        self.performSegue(withIdentifier: "showDetail", sender: articleModels)
    }
    // MARK: - func context menu
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let actionProvider: UIContextMenuActionProvider = { _ in
            let editMenu = UIMenu(title: "Share".localizedString(),image: UIImage(systemName: "square.and.arrow.up"), children: [
                UIAction(title: "Copy".localizedString()) { _ in },
                UIAction(title: "Duplicate".localizedString()) { _ in }
            ])
            return UIMenu(title: "What do you want to do?".localizedString(), children: [
                UIAction(title: "Delete".localizedString(),image: UIImage(systemName: "trash"),attributes: UIMenuElement.Attributes.destructive) { _ in
                    
                    let alert = UIAlertController(title: "", message: "Are you sure to delete this data?".localizedString(), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Yes".localizedString(), style: .destructive, handler: { _ in
                        // MARK: - delete data from api
                        ArticleViewModel.shared.deleteData(id: self.articleModels![indexPath.row].id!, completion:{ message in
                            self.articleModels?.remove(at: indexPath.row)
//                            self.articleTableView.reloadData()
                            //                            print("====Data has been delete from api====")
                            // MARK: - alert message from api
                            let delete = UIAlertController(title: nil, message: message.message, preferredStyle: .alert)
                            delete.addAction(UIAlertAction(title: "done".localizedString(), style: .default, handler: nil))
                            self.present(delete, animated: true, completion: nil)
                        })
                        
                    }))
                    alert.addAction(UIAlertAction(title: "No".localizedString(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                    //                    DispatchQueue.main.async {
                    //                        self.deleteData(id: self.articleModels![indexPath.row].id!)
                    //                        self.articleModels?.remove(at: indexPath.row)
                    //                        self.articleTableView.reloadData()
                    //                        print("====Data has been delete from api====")
                    
                    //                    }
                },
                UIAction(title: "Edit".localizedString(),image: UIImage(systemName: "pencil")){
                    _ in
                    // MARK: - push to update screen
                    let editView = self.storyboard?.instantiateViewController(identifier: "AddUpdateViewController") as! AddUpdateViewController
                    self.delegate = editView;
                    // MARK: - fetch data to update screen
                    self.delegate?.didSend(id:
                                            self.articleModels![indexPath.row].id!
                                           ,  title: self.articleModels![indexPath.row].title!
                                           , description: self.articleModels![indexPath.row]
                                            .description!, img: self.articleModels![indexPath.row]
                                                .image ?? "default", date: self.articleModels![indexPath.row]
                                                    .createdAt!, name: (self.articleModels![indexPath.row].author?.name) ?? "Jane",
                                           profile: self.articleModels![indexPath.row]
                                            .author?.imageProfile ?? "default", action: "update");
                    self.navigationController?.pushViewController(editView, animated: true)
                    
                },
                editMenu
            ])
        }
        
        return UIContextMenuConfiguration(identifier: "unique-ID" as NSCopying, previewProvider: nil, actionProvider: actionProvider)
    }
}


