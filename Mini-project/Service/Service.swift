//
//  Service.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/14/20.
//

import Foundation
import SwiftyJSON
import Alamofire
class Service {
    
    static let share = Service()
    
    let BASE_URL = "http://110.74.194.124:3000/api"
    
    // MARK: - fetch data to API
    func fetchDataTest(fetchpage: Int,completion: @escaping(_ articles: DataArticle) -> Void){
        
        let articleUrl = BASE_URL +  "/articles?page=\(fetchpage)&size=15"
        Alamofire.request(articleUrl, method:.get, parameters:nil, encoding:URLEncoding.default).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(DataArticle(json: json))
            case .failure(_):
                print("Fetch Failed")
            }
        }
    }
    func fetchData(completion: @escaping (_ articles: [Article]) -> Void){
        
        let articleUrl = BASE_URL +  "/articles?page=1&size=15"
        Alamofire.request(articleUrl, method:.get, parameters:nil, encoding:URLEncoding.default).responseJSON { (response) in
            switch response.result {
            case .success(_):
                // print(response.data)
                let json = try? JSON(data: response.data!)
                var articles = [Article]()
                let articleJson = json!["data"].array
                //                print(articleJson)
                for articleJsons in articleJson! {
                    let article = Article(json: articleJsons)
                    articles.append(article)
                }
                //                print("Service",articles)
                completion(articles)
                
            case .failure(_):
                print("Fetch Failed")
            }
        }
    }
    // MARK: - delete data to API
    func deleteData(id: String , completion: @escaping (Message)->())   {
        Alamofire.request("http://110.74.194.124:3000/api/articles/\(id)", method: .delete).responseData { (response) in
            switch response.result {            
            case .success(let value):
                do{
                    let decode =  try JSONDecoder().decode(Message.self, from: value)
                    completion(decode)
                }catch let error{
                    print("Error decode to message",error)
                }
            //                let json = JSON(value)
            //                let article = Article(json: json["data"])
            //                print("+++++++++++++++++++",value)
            
            case .failure(_):
                print("Failed to delete data")
            }
        }
    }
    // MARK: - add new data to API

    func addNewData(upload:Upload, completion: @escaping(Message)->()) {
        let urlPost = BASE_URL +  "/articles"
        let parameters: Parameters = [
            "title" : upload.title,
            "description" : upload.description,
            "published" : true,
            "image" : upload.image,
        ]
        Alamofire.request(urlPost, method: .post, parameters: parameters , encoding: JSONEncoding.default).responseData {response in
            switch response.result{
            
            case .success( let value):
                //                print(value)
                do{
                    let decode =  try JSONDecoder().decode(Message.self, from: value)
                    completion(decode)
                }catch let error{
                    print("Error decode to message",error)
                }
                
            case .failure(_):
                print("fail")
            }
            
        }
    }
    // MARK: - upload Image data to API
    func uploadImageToAPI(image: UIImage, completion: @escaping(ImageUpload)->()) {
        //        let imgData:Data = Data(UIImage(named: "pan3")!)
        //        let image = UIImage(named: image)
        let imgData = image.pngData()
        
        Alamofire.upload(multipartFormData:{ (multiform)
            in
            multiform.append(imgData!, withName: "image", fileName: ".jpg" , mimeType: "image/png")
        }, to: "http://110.74.194.124:3000/api/images", method:.post) { (result)
            in
            switch result{
            case .success(request: let request, streamingFromDisk: _, streamFileURL: _):
                request.responseData(completionHandler: { (response) in
                    if response.result.isSuccess {
                        //                        print("-----",response)
                        do{
                            let decode =  try JSONDecoder().decode(ImageUpload.self, from: response.data!)
                            completion(decode)
                        }catch let error{
                            print("Error decode to message",error)
                        }
                    }
                })
            case .failure(let err):
                print(err)
            }
        }
    }
    
    // MARK: - update data to API

    func updateData(id: String,upload:Upload, completion: @escaping(Message)->()) {
        let urlPost = BASE_URL +  "/articles/\(id)"
        let parameters: Parameters = [
            "title" : upload.title,
            "description" : upload.description,
            "published" : true,
            "image" : upload.image,
        ]
        Alamofire.request(urlPost, method: .patch, parameters: parameters , encoding: JSONEncoding.default).responseData {response in
            switch response.result{
            
            case .success( let value):
                do{
                    let decode =  try JSONDecoder().decode(Message.self, from: value)
                    completion(decode)
                }catch let error{
                    print("Error decode to message",error)
                }
                
            case .failure(_):
                print("fail")
            }
            
        }
    }
}
