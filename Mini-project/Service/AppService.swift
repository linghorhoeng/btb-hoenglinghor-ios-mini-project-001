//
//  AppService.swift
//  Coredata-Homework
//
//  Created by Hoeng Linghor on 12/6/20.
//

import Foundation
import UIKit

struct AppService {
    
    static let shared = AppService()
    
    var language: String {
        return UserDefaults.standard.string(forKey: "lang") ?? "en"
    }
    
    func choose(language: String){
        UserDefaults.standard.set(language, forKey: "lang")
    }
}

enum language: String {
    case english = "en"
    case khmer = "km"
}

