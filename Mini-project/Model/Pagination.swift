//
//  Pagination.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/14/20.
//

import Foundation
import SwiftyJSON
// MARK: - class Pagination
class Pagination {
    var page:Int?
    var limit:Int?
    var totalPages:Int?
    
    init() {}
    
    init(json:JSON){
        self.page = json["page"].int
        self.limit = json["limit"].int
        self.totalPages = json["total_page"].int
    }
}

