//
//  Upload.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/15/20.
//

import Foundation
import SwiftyJSON
// MARK: - Upload
struct Upload: Codable {
    let title, description: String
    let published: Bool
    let image: String

    enum CodingKeys: String, CodingKey {
        case title
        case description = "description"
        case published, image
    }
}
