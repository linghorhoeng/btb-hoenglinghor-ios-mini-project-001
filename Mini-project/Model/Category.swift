//
//  Category.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/14/20.
//

import Foundation
import SwiftyJSON
// MARK: - class Category
class Category {
    var id: Int?
    var name: String?
    
    init() { }

    init(json: JSON) {
        self.id = json["_id"].int
        self.name = json["name"].string
    }
    
}
