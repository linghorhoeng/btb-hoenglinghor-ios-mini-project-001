//
//  Article.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/14/20.
//

import Foundation
import SwiftyJSON

struct DataArticle {
    let data: [Article]
    let page: Int?
    let limit: Int?
    let totalPage: Int?
    let message: String
    
    init(json: JSON) {
        
        self.data = json["data"].arrayValue.compactMap(Article.init)
        self.page = json["page"].intValue
        self.limit = json["limit"].intValue
        self.totalPage = json["total_page"].intValue
        self.message = json["message"].stringValue
    }
    
}
// MARK: - class Article
class Article {
    var id:String?
    var title:String?
    var description:String?
    var image:String?
    var createdAt:String?
    var category: Category?
    var author: Author?

    
    init() {}
    
    init(json:JSON) {
        self.id = json["_id"].string
        self.title = json["title"].string
        self.description = json["description"].string
        self.image = json["image"].string
        self.createdAt = json["createdAt"].string
        self.category = Category(json: json["category"])
        self.author = Author(json: json["author"])
    }
}
