//
//  Author.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/15/20.
//

import Foundation
import SwiftyJSON
// MARK: - class Author
class Author {
    var id: Int?
    var name: String?
    var imageProfile:String?
    init() { }

    init(json: JSON) {
        self.id = json["_id"].int
        self.name = json["name"].string
        self.imageProfile = json["image"].string
    }
    
}
