//
//  ArticleModel.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/14/20.
//

import Foundation
import SwiftyJSON
// MARK: - class Article Model
class ArticleModel{
    var id:String?
    var title:String?
    var description:String?
    var image:String?
    var createdAt:String?
    var category: Category?
    var author: Author?
    
    init(article:Article) {
        self.id = article.id
        self.title = article.title
        self.description = article.description
        self.image = article.image
        self.createdAt = article.createdAt
        self.category = article.category
        self.author = article.author
        
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            if let date = dateFormat.date(from: article.createdAt!) {
                let newDate = DateFormatter()
                newDate.dateFormat = "dd-MMM-yyyy"
                self.createdAt = "Date: \(newDate.string(from: date))"
            
        }
        func checkImage( )-> String{
            if let url = article.image{
                return url
            }else{
                return "https://increasify.com.au/wp-content/uploads/2016/08/default-image.png"
            }
        }
    }
}
