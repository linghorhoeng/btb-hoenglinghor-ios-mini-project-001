//
//  Message.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/15/20.
//

import Foundation

// MARK: - Message
struct Message: Codable {
    let data: DataClass
    let message: String
}

// MARK: - DataClass
struct DataClass: Codable {
    let id, title, dataDescription: String
    let published: Bool?
    let image: String
    let createdAt, updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case title
        case dataDescription = "description"
        case published, image, createdAt, updatedAt
        case v = "__v"
    }
}
