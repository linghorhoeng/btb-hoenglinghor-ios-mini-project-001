//
//  ImageUpload.swift
//  Mini-project
//
//  Created by Hoeng Linghor on 12/16/20.
//

import Foundation
import SwiftyJSON
// MARK: - iamge upload
struct ImageUpload: Codable {
    let id: String
    let url: String
    let createdAt, updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case url, createdAt, updatedAt
        case v = "__v"
    }
}
